package controllers

import javax.inject.Inject
import javax.inject.Singleton

import model.ConfigsHandler
import play.api.libs.json.JsValue
import play.api.libs.json.Json
import play.api.libs.json.Writes
import play.api.mvc.AbstractController
import play.api.mvc.AnyContent
import play.api.mvc.ControllerComponents
import play.api.mvc.Request

/** This controller is responsible for handling all the interaction
  * with the configs and registering new users. */
@Singleton
class AgentController @Inject()(cc: ControllerComponents) extends AbstractController(cc)  {

  case class AccessKey(key: String)
  implicit val AccessKeyWriter = new Writes[AccessKey] {
    override def writes(o: AccessKey): JsValue = Json.obj(
      "key" -> o.key
    )
  }

  /** Registers a new user to the application, this method
    * will return the user's access key */
  def register() = Action { implicit request: Request[AnyContent] =>
    Ok(Json.toJson(AccessKey(ConfigsHandler.register())))
  }

  /** Add a new config file for the user with the given access key*/
  def addConfig(id: String, filename: String) = Action { implicit request: Request[AnyContent] =>
    ConfigsHandler.addConfig(id, filename, request.body.asJson.getOrElse(Json.toJson("empty")))
    Ok("Saved!!!")
  }

  /** Retrieve an existing config file */
  def retrieveConfig(id: String, filename: String) = Action { implicit request: Request[AnyContent] =>
    val contentStream = ConfigsHandler.retrieveConfig(id, filename)
    Ok(Json.parse(contentStream))
  }
}
