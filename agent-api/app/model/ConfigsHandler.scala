package model

import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.util.UUID

import play.api.libs.json.JsValue

object ConfigsHandler {
  /** Register a new client to be able to create
    * and retrieve configurations, this method will return
    * the client's access key */
  def register(): String = {
    val id = UUID.randomUUID().toString
    val uri = getClass.getClassLoader.getResource("configs")
    val registerPath = s"${uri.getPath}/$id"
    val file = new File(registerPath)
    file.mkdir()
    id
  }

  /** Add a new config file for the client with
    * the given access key */
  // TODO content should be json object
  def addConfig(id: String, filename: String, content: JsValue): Unit = {
    val uri = getClass.getClassLoader.getResource(s"configs/$id")
    val outputStream = new FileOutputStream(s"${uri.getPath}/$filename.json")
    outputStream.write(content.toString().getBytes())
    outputStream.close()
  }

  /** Retrieve an existing file from the user with the given
    * access key. */
  def retrieveConfig(id: String, filename: String): InputStream = {
    getClass.getClassLoader.getResourceAsStream(s"configs/$id/$filename.json")
  }
}
