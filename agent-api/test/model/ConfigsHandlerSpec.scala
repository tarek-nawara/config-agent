package model

import java.nio.charset.Charset

import org.apache.commons.io.IOUtils
import org.scalatest.FunSuite
import play.api.libs.json.Json

class ConfigsHandlerSpec extends FunSuite {
  test("ConfigsHandler register then adding a new config file") {
    val id = ConfigsHandler.register()
    val content = Json.toJson("""{"content": "hello world"}""")
    val filename = "config.json"
    ConfigsHandler.addConfig(id, filename, content)
    assert(id != null)
  }

  test("ConfigsHandler retrieve config should work") {
    val id = ConfigsHandler.register()
    val content = Json.toJson("""{"content": "hello world"}""")
    val filename = "config"
    ConfigsHandler.addConfig(id, filename, content)
    assert(id != null)

    val contentStream = ConfigsHandler.retrieveConfig(id, filename)
    assert(IOUtils.toString(contentStream, Charset.defaultCharset()) == content)
  }
}
